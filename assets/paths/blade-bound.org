*** Blade-Bound
Name:

Once you mark a bond with a kin-blade, a sentient weapon imbued with the spirit of your ancestor...

- [ ] When you [[file:~/other/ironsworn/moves/combat/enter-the-fray.org][Enter the Fray]] or [[file:~/other/ironsworn/moves/relationship/draw-the-circle.org][Draw the Circle]] while wielding your kin-blade, add +1 and take +1 momentum on a hit.

- [ ] When you [[file:~/other/ironsworn/moves/adventure/gather-information.org][Gather Information]] by listening to the whispers of your kin- blade, add +1 and take +2 momentum on a hit. Then, [[file:~/other/ironsworn/moves/suffer/endure-stress.org][Endure Stress]] (2 stress).

- [ ] When you [[file:~/other/ironsworn/moves/combat/strike.org][Strike]] with your kin-blade to inflict savage harm (decide before rolling), add +1 and inflict +2 harm on a hit. Then, [[file:~/other/ironsworn/moves/suffer/endure-stress.org][Endure Stress]] (2 stress).

