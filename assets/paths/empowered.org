*** Empowered

- [-] When you [[file:~/other/ironsworn/moves/relationship/sojourn.org][Sojourn]] and score a weak hit or miss, you may claim the rights of hospitality warranted by your title or lineage. If you do, roll all dice again and add +1. On a miss, you are refused, and your presumption causes significant new trouble.

- [ ] When you exert your title or lineage to [[file:~/other/ironsworn/moves/relationship/compel.org][Compel]], add +1 and take +1 momentum on a hit.

- [ ] When you forgo your title or lineage and [[file:~/other/ironsworn/moves/relationship/forge-a-bond.org][Forge a Bond]] as an equal, or when you [[file:~/other/ironsworn/moves/quest/swear-an-iron-vow.org][Swear an Iron Vow]] to serve someone of a lower station, add +1 and take +1 momentum or +1 spirit on a hit.
