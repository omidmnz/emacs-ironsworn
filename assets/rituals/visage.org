*** Visage

- [-] When you paint yourself in blood and ash, roll +wits. On a strong hit, you may add +2 and take +1 momentum on a hit when you [[file:~/other/ironsworn/moves/adventure/secure-advantage.org][Secure an Advantage]] or [[file:~/other/ironsworn/moves/relationship/compel.org][Compel]] using fear or intimidation. If you roll a 1 on your action die when making a move aided by your visage, the magic is spent. On a weak hit, as above, but the blood must be your own; [[file:~/other/ironsworn/moves/suffer/endure-harm.org][Endure Harm]] (2 harm).

- [ ] As above, and you may also add +1 when you [[file:~/other/ironsworn/moves/combat/strike.org][Strike]], [[file:~/other/ironsworn/moves/combat/clash.org][Clash]], or [[file:~/other/ironsworn/moves/combat/battle.org][Battle]].

- [ ] When you perform this ritual, add +1 and take +1 momentum on a hit.

