*** Young Wyvern
Name:

Your wyvern won't devour you. For now.

- [ ] *Insatiable:* When you [[file:~/other/ironsworn/moves/adventure/undertake-a-journey.org][Undertake a Journey]] and score a hit, you may suffer -1 supply in exchange for +2 momentum.

- [ ] *Indomitable:* When you make the [[file:~/other/ironsworn/moves/suffer/companion-endure-harm.org][Companion Endure Harm]] move for your wyvern, add +2 and take +1 momentum on a hit.

- [ ] *Savage:* When you [[file:~/other/ironsworn/moves/combat/strike.org][Strike]] by commanding your wyvern to attack, roll +heart. Your wyvern inflicts 3 harm on a hit.

[[elisp:(rpgdm-ironsworn-asset-stat-create "Wyvern Health" 4)][Add Wyvern Health to Character Sheet]]
