** Pay the Price

When you *suffer the outcome of a move*, choose one.

  - Make the most obvious negative outcome happen.
  - Envision two negative outcomes. Rate one as ‘likely’, and [[file:ask-the-oracle.org][Ask the Oracle]] using the yes/no table.
      On a ‘yes’, make that outcome happen.
      Otherwise, make it the other.
  - [[elisp:(rpgdm-tables-choose "pay-the-price")][Roll on the following table]]. If you have difficulty interpreting the result to fit the current situation, roll again.

|  Roll | Result                                                |
|-------+-------------------------------------------------------|
|   1-2 | Roll again and apply that result but make it worse.¹  |
|   3-5 | A trusted person or community loses faith in you²     |
|   6-9 | A cared for person or community is exposed to danger. |
| 10-16 | You are separated from something or someone.          |
| 17-23 | Your action has an unintended effect.                 |
| 24-32 | Something of value is lost or destroyed.              |
| 33-41 | The current situation worsens.                        |
| 42-50 | A new danger or foe is revealed.                      |
| 51-59 | It causes a delay or puts you at a disadvantage.      |
| 60-68 | It is harmful.                                        |
| 69-77 | It is stressful.                                      |
| 78-85 | A surprising development complicates your quest.      |
| 86-90 | It wastes resources.                                  |
| 91-94 | It forces you to act against your best intentions.    |
| 95-98 | A friend/companion/ally is in harm’s way.             |
| 99-00 | Roll twice more on this table. Both results occur.    |

¹ If you roll this result yet again, think of something dreadful that changes the course of your quest ([[file:ask-the-oracle.org][Ask the Oracle]] if unsure) and make it happen.

² Or the person or community acts against you.
*** Details
:PROPERTIES:
:VISIBILITY: folded
:END:

This is one of the most common moves in Ironsworn. Make this move when directed to by the outcome of another move, or when the current situation naturally leads to a cost through your choices or actions.

First, choose an option as described in the move. You may determine the outcome yourself, [[file:ask-the-oracle.org][Ask the Oracle]] to decide between two options, or roll on the table. In guided play, you look to your GM for a ruling. Whatever choice you make, always follow the fiction. If a dramatic outcome springs to mind immediately, go with it.

Next, envision the outcome. What happens? How does it impact the current situation and your character? Apply the outcome to the fiction of your scene before you determine any mechanical impact. Focusing on the narrative cost leads to deeper, more dramatic stories.

Finally, apply any appropriate mechanical penalty:

  - If you face a physical hardship or injury, [[file:../suffer/endure-harm.org][Endure Harm]] and suffer -health.
  - If you are disheartened or frightened, [[file:../suffer/endure-stress.org][Endure Stress]] and suffer -spirit.
  - If you lose equipment or exhaust resources, suffer -supply.
  - If you waste precious moments or are put in an unfavorable position, suffer -momentum.
  - If an ally or companion is put in harm’s way, apply the cost to them.

When in doubt, suffer -2 from the appropriate track.

Most situations can impact both the narrative situation and your mechanical status. But, a result might also be purely narrative without an immediate mechanical cost. An initial failure might introduce a complication or force a reactive move (such as [[file:../adventure/face-danger.org][Face Danger]]). A failure on a subsequent move can then introduce a mechanical penalty. In this way, failures build on each other, and the situation gets riskier and more intense.

The narrative and mechanical costs you endure should be appropriate to the circumstances and the move you are making. Scoring a *miss* on [[file:../combat/end-the-fight.org][End the Fight]] implies a greater cost than if you fail to [[file:../combat/clash.org][Clash]] within that scene. For dramatic moments and decisive moves, up the stakes.

Once you’ve resolved the outcome, envision what happens next and how you react. You are not in control. The situation is more complex and dangerous. You may need to respond with another move to restore your advantage and avoid further cost.
**** Rolling Matches
If you rolled a match on a move, and the outcome of that move tells you to Pay the Price, you can consider rolling on the table instead of just choosing an outcome. This fulfills the promise of the match by introducing a result you might otherwise not have considered. When in doubt about what a result on the table might represent (for example, “a new danger or foe is revealed”), you can [[file:ask-the-oracle.org][Ask the Oracle]]. However, rolling a match on the Pay the Price table itself doesn’t have any special significance.

#+STARTUP: showall
# Local Variables:
# eval: (flycheck-mode -1)
# End:
