#+TITLE: Dangers in a Tangled Wood

  - Denizen hunts
  - Denizen lairs here
  - Trap or snare
  - Path leads you astray
  - Entangling plant life
