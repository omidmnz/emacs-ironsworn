#+TITLE: Features of a Ravaged Land

Roll on Table: d20
  |   1-4 | Path of destruction                    |
  |   5-8 | Abandoned or ruined dwelling           |
  |  9-12 | Untouched or preserved area            |
  | 13-16 | Traces of what was lost                |
  | 17-20 | Ill-fated victims                      |
#+TITLE: Features of a Barrow

Roll on Table: d80 + 20
  | 21-43 | Burial chambers                  |
  | 44-56 | Maze of narrow passages          |
  | 57-64 | Shrine                           |
  | 65-68 | Stately vault                    |
  | 69-72 | Offerings to the dead            |
  | 73-76 | Statuary or tapestries           |
  | 77-80 | Remains of a grave robber        |
  | 81-84 | Mass grave                       |
  | 85-88 | Exhumed corpses                  |
  | 89-98 | Something unusual or unexpected  |
  |    99 | You transition into a new theme  |
  |   100 | You transition into a new domain |
