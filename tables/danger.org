#+TITLE:  Dangers
#+AUTHOR: Howard X. Abrams
#+EMAIL:  howard.abrams@gmail.com
#+DATE:   2021-12-22 December
#+TAGS:   rpg table ironsworn

Roll on Table: d100
|   1-30 | Check the theme card.                                       |
|  31-45 | Check the domain card.                                      |
|  46-57 | You encounter a hostile denizen.                            |
|  58-68 | You face an environmental or architectural hazard.          |
|  69-76 | A discovery undermines or complicates your quest.           |
|  77-79 | You confront a harrowing situation or sensation.            |
|  80-82 | You face the consequences of an earlier choice or approach. |
|  83-85 | Your way is blocked or trapped.                             |
|  86-88 | A resource is diminished, broken, or lost.                  |
|  89-91 | You face a perplexing mystery or tough choice.              |
|  92-94 | You lose your way or are delayed.                           |
| 95-100 | Roll twice more on this table. Both results occur.          |
